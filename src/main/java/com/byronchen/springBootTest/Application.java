package com.byronchen.springBootTest;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.filter.CharacterEncodingFilter;

@SpringBootApplication
@RestController
public class Application {
	
	/**
	 * @author  ByronChen
	 * @date 2016年7月10日下午2:05:34
	 * @description 简单模式：匹配多个URL
	 * @return
	 */
    @RequestMapping("/")
    public String greeting() {
        return "Hello World!";
    }
    @RequestMapping("/sayhello")
    public String sayHello() {
        return "Say Hello World!";
    }
    /**
     * @author  ByronChen
     * @date 2016年7月10日下午2:06:16
     * @description 配置模式：URL中的变量——PathVariable
     * @param username
     * @return
     */
    @RequestMapping("/users/{username}")
    public String userProfile(@PathVariable("username") String username) {
        return String.format("user %s", username);
    }

    @RequestMapping("/posts/{id}")
    public String post(@PathVariable("id") int id) {
        return String.format("post %d", id);
    }
    
    /**
     * @author  ByronChen
     * @date 2016年7月10日下午2:10:22
     * @description 支持HTTP方法,区分同一路径下不同的提交方式
     * @return
     */
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String loginGet() {
        return "Login Page";
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String loginPost() {
        return "Login Post Request";
    }
    @Bean
    public FilterRegistrationBean filterRegistrationBean() {
        FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
        characterEncodingFilter.setEncoding("UTF-8");
        registrationBean.setFilter(characterEncodingFilter);
        return registrationBean;
    }
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
