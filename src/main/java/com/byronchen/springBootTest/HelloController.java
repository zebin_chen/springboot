package com.byronchen.springBootTest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
/**
 * @author ByronChen
 * @description 模板渲染
 */

@Controller
public class HelloController {

    @RequestMapping("/hello/{name}")
    public String hello(@PathVariable("name") String name, Model model) {
        model.addAttribute("name", name);
        return "hello";
    }
    @RequestMapping("/hello2/{name}")
    public String helloTHY(@PathVariable("name") String name, ModelMap map) {
    	 // 加入一个属性，用来在模板中读取
        map.addAttribute("host", "http://blog.byronchen.com");
        // return模板文件的名称，对应src/main/resources/templates/index.html
        return "thyPage";  
    }
}
