# README #

这是个基本的springBoot版本，包括以下内容：

### 用途 ###

* 基本的springboot目录架构
* Version 1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### 基本功能? ###

* restful的API风格
* 添加了spring4的单元测试
* 添加了数据库版本的链接方式
* 具备了controller层，service层，dao层
* 使用了新模板thymeleaf
